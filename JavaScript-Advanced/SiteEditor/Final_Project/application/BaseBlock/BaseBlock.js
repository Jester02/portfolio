import {MyCourse} from '.././Classess/CourseClass';
import {News} from '.././Classess/NewsClass';
import {Menu} from '.././Classess/MenuAndLogo';

const CreateInitialBlock = ( Class, selector ) => {
    return () => {
        const ClassTargetBlock = document.getElementById( selector );
        let Instance = new Class();
        Instance.render( ClassTargetBlock );
            
        Instance.renderOptions()
    }
}
function readFile(input) {
  let file = input.srcElement.files[0];

  let reader = new FileReader();

  reader.readAsDataURL(file);

  reader.onload = function() {
    console.log(reader.result);
    MyLogo.setAttribute('src', reader.result);
    localStorage.setItem('Logo', reader.result);
  };

  reader.onerror = function() {
    console.log(reader.error);
  };
}
const BaseBlock = () => {
	const ChangeLogo = document.getElementById('ImageChanger');
	ChangeLogo.addEventListener('change', readFile);
	if (localStorage.getItem('Logo') == null){
		MyLogo.setAttribute('src','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQtZs-KpGcZfvbCVPrpwO9DdgS83RQXHJnajGbJQRC4ep3oqXYg');
	} else {
		MyLogo.setAttribute('src', localStorage.getItem('Logo'));
	}
	AddNewMenu.addEventListener('click', CreateInitialBlock( Menu, 'navigation-block' ) );
	AddNewNews.addEventListener('click' , CreateInitialBlock( News, 'NewsBlock') );
	AddNewCourse.addEventListener('click', CreateInitialBlock( MyCourse, 'CoursesBlock' ) );
}

	export {BaseBlock};
