import Vue from 'vue'
import VueRouter from 'vue-router'
import MainPage from '../views/MainPage.vue'
import GameShow from '../views/GameShow.vue'
import About from '../views/About.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Main',
    component:MainPage,
  },
  {
    name:'game',
    path:'/main/:id', 
    component:GameShow
  },
  {
    path: '/about',
    name: 'About',
    components:{
      default:() => import ('../views/About.vue'),
      navigation:() => import ('../views/MainPage.vue')
    }
  },
  {
    path:'/Favourites',
    name:'Favourites',
    components:{
      default:() => import('../views/Favourites.vue'),
      navigation:() => import ('../views/MainPage.vue')
    }
  }
  
]

const router = new VueRouter({
  routes
})

export default router
