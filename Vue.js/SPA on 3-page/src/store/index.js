import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    site:1,
    MainObj:{},
    MainArr:[],
    downloaded:true,
    proccess:true,
    slideShow:true,
    infoLoad:true,
    uniqObj:{},
    MyArr:[],
    AddMyGame:true
  },
  mutations: {
    SET_MAINARR(state, payload){
      state.MainArr = payload 
    },
    SET_MAINOBJ(state, payload){
      state.MainObj = payload
    },
    SET_DOWNLOADED(state, payload){
      state.downloaded = payload
    },
    SET_PROCCESS(state, payload){
      state.proccess = payload
    },
    SET_SLIDE_SHOW(state, payload){
      state.slideShow = payload
    },
    SET_SITE(state, payload){
      state.site = payload
    },
    SET_UNIQOBJ(state, payload){
      state.uniqObj = payload
    },
    SET_ADD_MYGAME(state, payload){
      state.AddMyGame = payload
    },
    SET_GAME_IN_MY_ARR(state, payload){
      state.MyArr.push(payload)
    },
    SET_MY_ARR(state,payload){
      state.MyArr = payload
    },
    SET_INFO_LOAD(state, payload){
      state.infoLoad = payload
    }
  },
  actions: {
    GET_MY_LIST({commit}, gameObj){
        if (gameObj.mark > 5){
          gameObj.mark = 5;
        }
        if (gameObj.mark < 1){
          gameObj.mark = 1
        }
        const Dupp = this.state.MyArr.filter(el => el.id === gameObj.id)
        if (Dupp.length === 1){
          const Undupp = this.state.MyArr.filter(el => el.id !== gameObj.id)
          commit('SET_MY_ARR', Undupp)
        } else {
          commit('SET_GAME_IN_MY_ARR',gameObj)
        }
    },
    GET_DEL_MYGAME({commit}, obj){
        const DelChoice = this.state.MyArr.filter(el => el.id !== obj.id)
        commit('SET_MY_ARR', DelChoice);
    },
    GET_INFO({commit}){
      commit('SET_PROCCESS', false)
        const ReceiveObj = (data) => { 
          commit('SET_MAINOBJ' , data)
          commit('SET_MAINARR', data.results)
        }
        fetch("https://rawg-video-games-database.p.rapidapi.com/games?page="+this.state.site+"", {
	        "method": "GET",
	        "headers": {
		          "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com",
		          "x-rapidapi-key": "160d7e77cemshe96a4528fb59fd9p103399jsnaaf023e18593"
	        }
          }).then(response => 
            response.json())
            .then(ReceiveObj)
            .catch(err => {
	          console.log(err);
          });
    },
    GET_GAME({commit} , id){
        const ReceiveObj = (data) => { 
          commit('SET_UNIQOBJ',data);
        }
        fetch("https://rawg-video-games-database.p.rapidapi.com/games/"+id, {
	        "method": "GET",
	        "headers": {
		          "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com",
		          "x-rapidapi-key": "160d7e77cemshe96a4528fb59fd9p103399jsnaaf023e18593"
	        }
          }).then(response => 
          response.json())
          .then(ReceiveObj)
          .catch(err => {
	        console.log(err);
          });
          commit('SET_INFO_LOAD',false)
    }
  },
  modules: {
  }
})
